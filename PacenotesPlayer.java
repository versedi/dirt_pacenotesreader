import ddf.minim.AudioPlayer;
import ddf.minim.Minim;
import java.util.TreeMap;

public class PacenotesPlayer {


    private TreeMap<Integer, String[]> stagePacenotes;
    public AudioPlayer wrongWayPlayer;
    public AudioPlayer countDownPlayer;
    protected AudioPlayer[] player;
    public int currentDistance, lastPlayedDistance, lastPlayedWrongWay;
    public String extension;
    public int lastCheckedDistance;
    boolean countDownPlayed;
    UdpReceiver udpReceiver;
    PacenotesReader pacenotesReader;
    Minim minim;
    public PacenotesPlayer(PacenotesReader pacenotesReader, UdpReceiver udpReceiver, Minim minim, String soundExtension) {
        this.udpReceiver = udpReceiver;
        this.minim = minim;
        this.extension = soundExtension;
        this.pacenotesReader = pacenotesReader;
        lastPlayedDistance = 0;
        countDownPlayer = minim.loadFile("\\data\\sounds\\countdown_start" + extension);
        wrongWayPlayer = minim.loadFile("\\data\\sounds\\wrong_way" + extension);
    }

    public void setStagePacenotes(TreeMap<Integer, String[]> pacenotes) {
        stagePacenotes = pacenotes;
    }

    public void read() {
        if(udpReceiver.distance < 0) {
            this.currentDistance = 0; //current distance in polo = -1
        } else {
            this.currentDistance = (int) udpReceiver.distance;
        }


        if(isTimerStarted()) {
            stopCountDown();
        }
        if (isStartLine()) {
            if (isHandBrakePulled()) {
                playCountDown();
            }
        }
        if (!isGoingWrongWay()) {
            if (this.lastCheckedDistance != this.currentDistance) {
                playRowPacenotes();
            }
        } else {
            playWrongWay();
        }

        if(isStageRestarted() || isStageFinished()) {
            newRun();
        }
    }

    private boolean isTimerStarted() {
        return udpReceiver.lapTime > 0;
    }
    private void stopCountDown() {
        countDownPlayer.pause();
        countDownPlayer.rewind();
    }

    private boolean isStageFinished() {
        return this.currentDistance >= (int) udpReceiver.overallStageDistance;
    }

    private boolean isStageRestarted() {
        return (udpReceiver.lapTime == 0 && this.lastPlayedDistance > currentDistance);
    }

    public void newRun() {
        countDownPlayer.rewind();
        countDownPlayed = false;
        lastPlayedDistance = 0;
        pacenotesReader.selectedStageName = null;
    }

    public boolean isHandBrakePulled() { // @TODO: Figure a way to detect this!
        if(udpReceiver.brakes == 0.0 && udpReceiver.speed > 0.2 && udpReceiver.clutch == 1.0) {
            return false;
        }
        return udpReceiver.brakes == 0.0 && udpReceiver.speed < 1 && udpReceiver.clutch ==  1.0;
    }

    public boolean isStartLine() {
        int roundedStageStartLine = pacenotesReader.round(this.pacenotesReader.getStageStartLinePosX());
        pacenotesReader.println("roundedStageStartLine " +roundedStageStartLine  );
        if(pacenotesReader.round(udpReceiver.positionX) == roundedStageStartLine || pacenotesReader.round(udpReceiver.positionX) - 1 == roundedStageStartLine || pacenotesReader.round(udpReceiver.positionX) + 1 == roundedStageStartLine) {
            return true;
        }
//        if (
//                udpReceiver.distance > -1.01
//                && udpReceiver.distance < -0.75 &&
//                currentDistance == 0 &&
//                this.lastPlayedDistance == currentDistance &&
//                udpReceiver.speed < 0.03000000 &&
//                udpReceiver.clutch == 1.0) {
//            startLine = true;
//            countDownPlayed = false;
//            return true;
//        }
        return false;
    }

    public void playCountDown() {

        if (!countDownPlayed) {
            countDownPlayer.play();
            countDownPlayed = true;
        }
    }


    public boolean isGoingWrongWay() {
        return udpReceiver.distance > 0 &&
                this.lastPlayedDistance > this.currentDistance;
    }

    public void playWrongWay() {
        boolean playAgain = this.currentDistance < this.lastPlayedDistance - 20;
        if (playAgain) {
            if (!wrongWayPlayer.isPlaying()) {
                wrongWayPlayer.play();
                this.lastPlayedWrongWay = (int) udpReceiver.distance;
            }
        }
    }

    public void playRowPacenotes() {
        this.lastCheckedDistance = this.currentDistance;

        if (this.currentDistance > this.lastPlayedDistance) {
            String[] rowPacenotes = stagePacenotes.get(this.currentDistance);
            if (rowPacenotes != null) {
                Integer soundCount = rowPacenotes.length;
                player = new AudioPlayer[soundCount];
                for (int s = 0; s < soundCount; s++) {
                    player[s] = minim.loadFile("\\data\\sounds\\" + rowPacenotes[s] + extension); // @TODO add config dropdown with extension for sounds
                }
                if (player.length == 1) {
                    playSingleSound(player[0]);
                } else {
                    playMultipleSounds(player);
                }

            }
        }
    }

    public void playSingleSound(AudioPlayer player) {
        try {
            player.play();
            while (player.isPlaying()) {
                pacenotesReader.delay(player.length() - player.position());
            }
//            if(!player.isPlaying()) {
//                player.play();
//                this.lastPlayedDistance = currentDistance;
//            } else {
//                player.close();
//            }
        } catch (Exception e) {

        }
    }

    public void playMultipleSounds(AudioPlayer[] player) {
        for (int soundIndex = 0; soundIndex < player.length; soundIndex++) {
            if (soundIndex == 0) {
                try {
                    player[0].play();
                    this.lastPlayedDistance = this.currentDistance;
                    while (player[0].isPlaying()) {
                        pacenotesReader.delay(player[0].length() - player[0].position());
                    }
                } catch (Exception e) {

                }
            } else if (soundIndex > 0) {
                while (player[soundIndex - 1].isPlaying()) {
                    pacenotesReader.delay(player[soundIndex - 1].length() - player[soundIndex - 1].position());
                }
                try {
                    player[soundIndex].play();
                    this.lastPlayedDistance = this.currentDistance;
                } catch (Exception e) {

                }
            }
        }
    }
}
