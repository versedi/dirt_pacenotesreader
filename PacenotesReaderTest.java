/**
 * Created by versedi on 2016-12-03.
 */

import org.junit.Test;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class PacenotesReaderTest {
    PacenotesReader pacenotesReader;


    @Test
    public void selectedStageIsEmpty()  throws Exception {
        pacenotesReader = new PacenotesReader();

        assertEquals(null, pacenotesReader.selectedStageName);
    }

}
