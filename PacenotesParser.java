import java.util.TreeMap;

public class PacenotesParser {
    private TreeMap<Integer, String[]> parsedPacenotes;
    private String[] pacenotesContent;

    PacenotesParser() {
        parsedPacenotes = new TreeMap<>();

    }

    public void setStageAndParse(String[] pacenotesContent) {
        this.pacenotesContent = pacenotesContent;
        parsePacenotes();
    }

    private void parsePacenotes() {
        for(int r = 1; r < pacenotesContent.length; r++) {
            try {
                String[] row = pacenotesContent[r].split(",");

                Integer distance = Integer.parseInt(row[0]);
                String soundsString = row[1];
                String[] soundNames = soundsString.split(" ");
                int soundsCount = soundNames.length;
                //@TODO Remove below replacing.
                for (int s = 0; s < soundsCount; s++) {
                    soundNames[s] = soundNames[s].replace(".wav", "");
                    soundNames[s] = soundNames[s].replace(".mp3", "");
                }
                parsedPacenotes.put(distance, soundNames);
            } catch (Exception e) {
                e.getCause();
            }
        }
    }

    public TreeMap<Integer, String[]> getParsedPacenotes() {
        parsePacenotes();
        return parsedPacenotes;
    }
}