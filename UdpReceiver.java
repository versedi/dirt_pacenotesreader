import hypermedia.net.UDP;

public class UdpReceiver extends PacenotesReader {
    protected boolean debugging = true;

    protected UDP udpRX;
    protected boolean udpOpen;
    protected int portRX = 10001;
    protected String ip = "127.0.0.1";

    public double speed;
    public float brakes, clutch, distance;
    public float lapTime, tTime;

    public float overallStageDistance, positionX; //used for stage detector
    UdpReceiver() {
        createUDP();
    }

    void createUDP() {
        // Create new object for receiving
        udpRX = new UDP(this, portRX, ip);
        udpRX.log(false);
        udpRX.listen(true);
        udpOpen = true;
        if (debugging) System.out.print("UDP Opened");
    }

    public void closeUDP() {
        udpRX.dispose();
        udpOpen = false;
        if (debugging) System.out.print("UDP Closed");
    }

    public void resetAll() {
        closeUDP();
        createUDP();
    }

    /*
    * @author: Original version author: Zeb
    * @source: DiRT Telemetry Tool
    */
    public void receive(byte[] data, String ip, int portRX) {
        //Bitwise & ([pos] & 0xff), Takes byte and multiplies it by 00000000 00000000 11111111, so that the only thing remaining are the last 8 bits.
        //Float.intBitsToFloat converts binary to a float integer and makes it equal to the stated vars.
        int pos = 0;
        tTime = Float.intBitsToFloat((data[pos] & 0xff) | ((data[pos + 1] & 0xff) << 8) | ((data[pos + 2] & 0xff) << 16) | ((data[pos + 3] & 0xff) << 24));
        String time = timeConversion((int) (tTime * 100)); // wrong - this counts from where loading ends, isnt resetted and goes on when you pause the game.
        // Lap time
        pos = 4;
        lapTime = Float.intBitsToFloat((data[pos] & 0xff) | ((data[pos + 1] & 0xff) << 8) | ((data[pos + 2] & 0xff) << 16) | ((data[pos + 3] & 0xff) << 24));
        String lapTimeString = timeConversion((int) (lapTime * 100)); // starts when you start.
        // Distance on stage
        pos = 8;
        distance = Float.intBitsToFloat((data[pos] & 0xff) | ((data[pos + 1] & 0xff) << 8) | ((data[pos + 2] & 0xff) << 16) | ((data[pos + 3] & 0xff) << 24));
        // Position X on stage
        pos = 16;
        positionX = Float.intBitsToFloat((data[pos] & 0xff) | ((data[pos + 1] & 0xff) << 8) | ((data[pos + 2] & 0xff) << 16) | ((data[pos + 3] & 0xff) << 24));
        //Speed
        pos = 28;
        speed = Float.intBitsToFloat((data[pos] & 0xff) | ((data[pos + 1] & 0xff) << 8) | ((data[pos + 2] & 0xff) << 16) | ((data[pos + 3] & 0xff) << 24)) * 3.6;
        //Brakes 0-1
        pos = 124;
        brakes = Float.intBitsToFloat((data[pos] & 0xff) | ((data[pos + 1] & 0xff) << 8) | ((data[pos + 2] & 0xff) << 16) | ((data[pos + 3] & 0xff) << 24));//Brakes 0-1
        //Clutch 0-1
        pos = 128;
        clutch = Float.intBitsToFloat((data[pos] & 0xff) | ((data[pos + 1] & 0xff) << 8) | ((data[pos + 2] & 0xff) << 16) | ((data[pos + 3] & 0xff) << 24));
        //Overall stage distance
        pos = 244;
        overallStageDistance = Float.intBitsToFloat((data[pos] & 0xff) | ((data[pos + 1] & 0xff) << 8) | ((data[pos + 2] & 0xff) << 16) | ((data[pos + 3] & 0xff) << 24));

        if (debugging)
            printUdpOutput();

    }


    public static String timeConversion(int centiseconds) {

        final int SECONDS_IN_A_MINUTE = 60;
        final int CENTISECONDS_IN_A_SECOND = 100;

        int seconds = centiseconds / CENTISECONDS_IN_A_SECOND;
        centiseconds -= seconds * CENTISECONDS_IN_A_SECOND;

        int minutes = seconds / SECONDS_IN_A_MINUTE;
        seconds -= minutes * SECONDS_IN_A_MINUTE;

        return nf(minutes, 2) + ":" + nf(seconds, 2) + ":" + nf(centiseconds, 2);
    }

    public void printUdpOutput() {
        System.out.print("speed: " + speed + "\n");
        System.out.print("brakes: " + brakes + "\n");
        System.out.print("clutch: " + clutch + "\n");
//        System.out.print("distance: " + distance + "\n");
        System.out.print("positionX: " + positionX + "\n");
//        System.out.print("brakes: " + brakes + "\n");
//        System.out.print("currentDistance: " + currentDistance + "\n");
//        System.out.print("lapTime: " + lapTime + "\n");
//        System.out.print("overallStageDistance: " + overallStageDistance + "\n");
//        System.out.print("lapTimeString: " + lapTimeString + "\n");
    }
}
