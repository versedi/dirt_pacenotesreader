import java.io.File;
import java.util.TreeMap;

public class FilesCollector {
    protected boolean debugging = true;

    protected TreeMap<String, String> filesCollection;
    protected File[] filesList;

    public FilesCollector() {
        filesCollection = new TreeMap<String, String>();
        filesList = new File[0];
    }

    public void collectFiles(File directory) {
        File[] list = directory.listFiles();
        if(list == null) {
            // directory is empty !
            // display warning/error message on bottom
            return;
        }

        filesList = list;
        for (File file : filesList) {
            if (file.isFile()) {
                insertFileIntoCollection(file);
            }
        }
    }

    private void insertFileIntoCollection(File file) {
        String fileName = file.getName();
        String filePath = file.getPath();
        filesCollection.put(fileName, filePath);
        if(debugging) {
            System.out.print("Adding " + filePath + " to filesCollection, index:" + fileName + "\n");
        }
    }


    public TreeMap<String, String> getFilesCollection() {
        return filesCollection;
    }

}
