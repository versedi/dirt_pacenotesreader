import controlP5.*;
import ddf.minim.Minim;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

import java.io.File;
import java.util.Objects;
import java.util.TreeMap;


public class PacenotesReader extends PApplet {

    public ControlP5 cp5;
    public boolean debugging = true;
    boolean startLine;
    String stageTime, selectedStageName, selectedSoundExt = ".wav";
    private ScrollableList stagesDropdown;
    private DropdownList soundExtension;
    private String[] pacenotesFiles;
    private PacenotesCollector pacenotesCollector;
    private SoundsFilesCollector soundsFilesCollector;
    private TreeMap<String, String> soundsFilesCollection;
    private TreeMap<Integer, String[]> pacenotes;
    UdpReceiver udpReceiver;
    private PacenotesPlayer pacenotesPlayer;
    private Controller<?> distanceText;
    Minim minim;
    private PImage background;
    public processing.data.Table stagesByDistance;
    private StageDetector stageDetector;
    private boolean userSelectedStage = false;

    public Float getStageStartLinePosX() {
        return this.stageStartLinePosX;
    }

    public Float stageStartLinePosX;

    public static void main(String[] args) {
        PApplet.main(new String[]{"PacenotesReader"});
    }

    private static final File SOUNDS_DIR = new File("data/sounds/");
    private static final File PACENOTES_DIR = new File("data/pacenotes/");
    //	An array of stripes

    public void settings() {


        minim = new Minim(this);
        udpReceiver = new UdpReceiver();
        stagesByDistance = loadTable("\\stages.csv", "header, csv");
        stageDetector = new StageDetector(stagesByDistance); //<>//
        soundsFilesCollector = new SoundsFilesCollector(SOUNDS_DIR);
        pacenotesCollector = new PacenotesCollector(PACENOTES_DIR);
        pacenotesFiles = pacenotesCollector.getStages();
        pacenotesPlayer = new PacenotesPlayer(this, udpReceiver, minim, selectedSoundExt);
        size(400, 700, P3D);
    }

    public void setup() {
        cp5 = new ControlP5(this);
        createUi();
    }


    public void receive(byte[] data, String ip, int portRX) { //<>//
        udpReceiver.receive(data, ip, portRX); //<>//
    }

    public void draw() {
        background(background);

        smooth();
        if (isUserInMenuNoStageSelected()) {
            selectedStageName = null;
            stagesDropdown.setCaptionLabel("Detecting stage...");
        } else if(selectedStageName != null && userSelectedStage && this.stageStartLinePosX == null) {
            stageDetector.detectedStageName = selectedStageName;
            this.stageStartLinePosX = stageDetector.getDetectedStageStartLine();
        }
        if (selectedStageName != null && udpReceiver.udpOpen && this.stageStartLinePosX != null) {
            updateUi();
            pacenotesPlayer.read();
        } else if (udpReceiver.udpOpen && udpReceiver.overallStageDistance != 0.0) {
            selectedStageName = stageDetector.findStages(udpReceiver.overallStageDistance, udpReceiver.positionX);
            if (!Objects.equals(selectedStageName, "") && selectedStageName != null) {
                this.stageStartLinePosX = stageDetector.getDetectedStageStartLine();
                stagesDropdown.setCaptionLabel("Detected stage: " + selectedStageName);
//				Map item = cp5.get(ScrollableList.class, "selectStageDropdown").getItem(selectedStageName);
//				println(item);
                println("Automatically detected stage: " + selectedStageName);
                collectPacenotes();
            }

        }
    }

    private boolean isUserInMenuNoStageSelected() {
        if (!userSelectedStage && udpReceiver.speed == 0.0 && udpReceiver.clutch == 0.0 && udpReceiver.positionX == 0.0) {
            return true;
        }
        return false;
    }

    public void stop() {
        pacenotesPlayer.countDownPlayer.close();
        pacenotesPlayer.wrongWayPlayer.close();
        pacenotesPlayer.wrongWayPlayer.close();
        udpReceiver.closeUDP();
        minim.stop();
        super.stop();
    }

    public void updateUi() {
        cp5.getController("distance").setStringValue(nf(udpReceiver.distance, 5, 8));
        cp5.getController("overallDistance").setStringValue(nf(udpReceiver.overallStageDistance, 2, 2));

//		cp5.getController("message").setStringValue(message); @TODO Bar with error messages
    }


    //ControlP5 callback - method name must be the same as the string parameter of cp5.addScrollableList()
    public void selectStageDropdown(int n) {
  /* request the selected item based on index n */
        selectedStageName = stagesDropdown.getItem(n).get("name").toString();
        stagesDropdown.setCaptionLabel("Selected stage: " + selectedStageName);
        println("User selected stage: " + selectedStageName);
        userSelectedStage = true;
        collectPacenotes();
    }


    public void collectPacenotes() {
        PacenotesParser pacenotesParser = new PacenotesParser(); //<>//

        String[] stageContent = pacenotesCollector.getPacenotesFileContent(selectedStageName); //<>//
        if (stageContent != null) {
            pacenotesParser.setStageAndParse(stageContent);
            pacenotes = pacenotesParser.getParsedPacenotes();
            pacenotesPlayer.setStagePacenotes(pacenotes);
        }
    }

    public void createResetCountdownButton(int n) {
        pacenotesPlayer.countDownPlayed = false;
    }

    public void selectSoundExtension(int n) {
        selectedSoundExt = cp5.get(DropdownList.class, "selectSoundExtension").getItem(n).get("name").toString();
        cp5.get(DropdownList.class, "selectSoundExtension").setLabel(selectedSoundExt);
    }


    public void createUi() {
        background = loadImage("data/background.jpg");
        PFont contentFont = createFont("arial", 14, true);
        PFont headingsFont = createFont("verdana bold", 16, true);
        PFont controlsFont = createFont("verdana bold", 24, true);
        cp5.setFont(contentFont);

        createStagesDropdown(contentFont, headingsFont);
        createCurrentDistance(controlsFont);
        createStageOverallDistance(contentFont, controlsFont);
        createSoundExtensionDropdown(headingsFont);
        createResetCountdownButton(headingsFont);
    }


    private void createStageOverallDistance(PFont contentFont, PFont controlsFont) {
        cp5.addTextlabel("lblcurrentStageOverallDistance")
                .lock()
                .setSize(65, 25)
                //.setDecimalPrecision(1)
                .setPosition(25, 500)
                .setValue("Overall stage distance: ")
                .setColorBackground(color(0, 0, 0))
                .setVisible(true)
                .setFont(contentFont);
        cp5.getController("lblcurrentStageOverallDistance").getCaptionLabel().setVisible(false);
        //cp5.getController("distance").getValueLabel().setFont(p1);

        cp5.addTextlabel("overallDistance")
                .lock()
                .setSize(65, 25)
                //.setDecimalPrecision(1)
                .setPosition(180, 500)
                .setValue("No UDP Received yet.")
                .setColorBackground(color(0, 0, 0))
                .setVisible(true)
                .setFont(contentFont);
        distanceText.getCaptionLabel().setVisible(false);
        distanceText.getValueLabel().setFont(controlsFont);
    }

    private void createCurrentDistance(PFont controlsFont) {
        cp5.addTextlabel("lbldistance")
                .lock()
                .setSize(65, 25)
                //.setDecimalPrecision(1)
                .setPosition(25, 460)
                .setValue("Current distance (m)")
                .setColorBackground(color(0, 0, 0))
                .setVisible(true);
        cp5.getController("lbldistance").getCaptionLabel().setVisible(false);
        //cp5.getController("distance").getValueLabel().setFont(p1);

        distanceText = cp5.addTextlabel("distance")
                .lock()
                .setSize(65, 25)
                //.setDecimalPrecision(1)
                .setPosition(180, 455)
                .setValue("00000,00")
                .setColorBackground(color(0, 0, 0))
                .setVisible(true);
        distanceText.getCaptionLabel().setVisible(false);
        distanceText.getValueLabel().setFont(controlsFont);
    }

    private void createStagesDropdown(PFont contentFont, PFont headingsFont) {
        cp5.addTextlabel("selectStage")
                .setText("Select a stage or wait for \n auto-detection")
                .setPosition(25, 100)
                .setColorValue(255)
                .setFont(headingsFont);

        stagesDropdown = cp5.addScrollableList("selectStageDropdown")
                .setLabel("No stage selected, detecting...")
                .setPosition(25, 150)
                .setSize(350, 300)
                .setBarHeight(25)
                .setItemHeight(25)
                .addItems(pacenotesFiles)
                .setOpen(false)
                .setType(ScrollableList.LIST);
        ;
        stagesDropdown.setBackgroundColor(color(0));
        stagesDropdown.setColorBackground(color(60));
        stagesDropdown.setColorActive(color(255, 0, 0));
        stagesDropdown.getValueLabel().setFont(contentFont);
    }

    private void createResetCountdownButton(PFont headingsFont) {
        Button b = cp5.addButton("resetCountdownPlayed")
                .setLabel("Reset countdown")
                .setPosition(20, 550)
                .setSize(200, 17)
                ;
        b.getCaptionLabel().setFont(headingsFont);
    }


    private void createSoundExtensionDropdown(PFont headingsFont) {
        cp5.addTextlabel("selectSound")
                .setText("Select sound extension")
                .setPosition(20, 585)
                .setColorValue(255)
                .setFont(headingsFont);


        String[] extensions = new String[3];
        extensions[0] = ".wav";
        extensions[1] = ".mp3";
        extensions[2] = ".auf";

        soundExtension = cp5.addDropdownList("selectSoundExtension")
                .setLabel(".wav")
                .setPosition(20, 610)
                .setSize(250, 100)
                .setBarHeight(25)
                .setItemHeight(25)
                .addItems(extensions)
                .setOpen(false)
                .setValueSelf(0)
                .setType(DropdownList.LIST);// currently supported DROPDOWN and LIST
        ;
        soundExtension.setBackgroundColor(color(0));
        soundExtension.setColorBackground(color(60));
        soundExtension.setColorActive(color(255, 0, 0));
    }
}
