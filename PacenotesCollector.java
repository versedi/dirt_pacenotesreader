import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class PacenotesCollector extends FilesCollector {


    protected TreeMap<String, String[]> pacenotesCollection = new TreeMap<String, String[]>();
    protected String[] stagesNames;

    public PacenotesCollector(File pacenotesDir) {
        collectFiles(pacenotesDir);
        filesCollection = getFilesCollection();
    }

    private void collect() {
        if (filesCollection.size() > 0) {
            stagesNames = new String[filesCollection.size()];
            int s = 0;
            for(Map.Entry<String,String> entry : filesCollection.entrySet()) {
                File file = new File(entry.getValue());
                String stageName = entry.getKey().replace(".txt", "");
                processPacenoteFile(file, stageName);
                stagesNames[s] = stageName;
                s++;
            }
        }
    }

    private void processPacenoteFile(File file, String stageName) {
        if (file.isFile()) {
            String pacenoteFilePath = file.getPath();
            try {
                List fileRows = getFileRowsAsList(file);
                String[] pacenotes = (String[]) fileRows.toArray(new String[fileRows.size()]);
                pacenotesCollection.put(stageName, pacenotes);
                if (debugging)
                    System.out.print("Adding " + stageName + " - " + pacenoteFilePath + " to pacenotesFilesCollection, index:" + stageName + "\n");
            } catch (IOException e) {
                System.out.print(e.getCause());
            }

        }
    }


    public List getFileRowsAsList(File pacenoteFile) throws IOException {
        return FileUtils.readLines(pacenoteFile, "utf-8");
    }

    public String[] getStages() {
        collect(); //<>//

        return stagesNames;
    }

    public String[] getPacenotesFileContent(String selectedStage) {
        return pacenotesCollection.get(selectedStage);
    }


}
