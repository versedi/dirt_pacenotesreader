import processing.data.Table;
import processing.data.TableRow;

import java.util.TreeMap;

/*
 * Pacenotes Reader for DiRT Rally
 *
 * @author: versedi
 */
public class StageDetector extends PacenotesReader {

    public String detectedStageName;
    Table stagesByDistance;

    public StageDetector(Table stagesByDistance) {
        this.stagesByDistance = stagesByDistance;
    }

    public String findStages(Float overallStageDistance, Float positionX) {
        Iterable<TableRow> matchingStages = stagesByDistance.findRows(String.valueOf(overallStageDistance), "overallStageDistance");
        int detectedByDistance = 0;
        String[] stages = new String[2];
        TreeMap<String, Float> stagesByPosX = new TreeMap<String, Float>();

        for (TableRow stage : matchingStages) {
            stages[detectedByDistance] = stage.getString("stageName");
            stagesByPosX.put((String) stage.getString("stageName"), (Float) stage.getFloat("positionXStartLine"));
            detectedByDistance++;
        }
        if (detectedByDistance == 1) {
            detectedStageName = stages[0];
        }
        if (detectedByDistance > 1) {
            switchStageNameAndPosition(stages, positionX);
        }

        return detectedStageName;
    }

    private void switchStageNameAndPosition(String[] stages, float positionX) {
        switch (stages[0]) {
            case "Dyffryn Afon":
                if (positionX < 0) {
                    detectedStageName = "Dyffryn Afon";
                } else {
                    detectedStageName = "Dyffryn Afon Reverse";
                }
                break;
            case "Dyffryn Afon Reverse":
                if (positionX > 0) {
                    detectedStageName = "Dyffryn Afon Reverse";
                } else {
                    detectedStageName = "Dyffryn Afon";
                }
                break;
            case "Gordolon - Courte Montee":
                if (positionX < -1800) {
                    detectedStageName = "Gordolon - Courte Montee";
                } else {
                    detectedStageName = "Col de Turini - Descente";
                }
                break;
            case "Col de Turini - Descente":
                if (positionX > -500) {
                    detectedStageName = "Col de Turini - Descente";
                } else {
                    detectedStageName = "Gordolon - Courte Montee";
                }
                break;
        }
    }

    public Float getDetectedStageStartLine() {
        TableRow row = stagesByDistance.findRow(detectedStageName, "stageName");

        return row.getFloat("positionXStartLine");
    }

}
