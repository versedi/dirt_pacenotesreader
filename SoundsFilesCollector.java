import java.io.File;
import java.util.TreeMap;

public class SoundsFilesCollector extends FilesCollector {
    private TreeMap<String,String> soundsFilesCollection = new TreeMap<String, String>();
    String COUNTDOWN = "countdown_start";
    String WRONGWAY = "wrong_way";
    // @Todo validation for required sounds above
    public SoundsFilesCollector(File soundDirectory) {
        collectFiles(soundDirectory);
        soundsFilesCollection = getFilesCollection();
    }

}
